class Product{
    String name;
    double price;
    int rating;

    Product(String name, double price, int rating){
        this.name = name;
        this.price = price;
        this.rating = rating;
    }

    public  String getName(){
        return name;
    }
    public  double getPrice(){
        return price;
    }
    public  int getRating(){
        return rating;
    }
    public void setProduct(String name, double price, int raring)
    {
        this.name=name;
        this.price=price;
        this.rating=rating;
    }

}

class Category{
    String name;
    Product[] products;

    Category(String name, Product[] products){
        this.name = name;
        this.products = new Product[products.length];
        for(int i = 0;i<products.length;i++){
            this.products[i] = new Product(products[i].getName(),products[i].getPrice(),products[i].getRating());
        }
    }
}

class Basket{
    Product[] products;
    Basket(Product[] products){

        this.products = new Product[products.length];
        for(int i = 0;i<products.length;i++){
            this.products[i] = new Product(products[i].getName(),products[i].getPrice(),products[i].getRating());
        }
    }
    public Product[] getProducts(){
        return products;
    }
    public int getSize(){
        return products.length;
    }
}

class User{
    String login;
    String password;
    Basket basket;

    User(String login, String password, Basket basket){
        this.login = login;
        this.password = password;
        this.basket = basket;


    }
}

public class Main {
    public static void main(String[] args) {

        Product[] products = new Product[2];
        products[0] = new Product("pita",80, 10);
        products[1] = new Product("bread",42.4, 7);

        Product[] products2 = new Product[3];
        products2[0] = new Product("chicken",150.6, 7);
        products2[1] = new Product("beef",324.9, 8);
        products2[2] = new Product("pork",350.2, 9);





        Category meat = new Category("meat", products2);
        Category bread_products = new Category("bread_products", products);

        Basket basket = new Basket(products);
        User me = new User("Login", "Password", basket);

    }
}

